package world.code.sprint.fourth;

import java.util.Scanner;

public class MinimumDistance {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);

		int arrayLength = scanner.nextInt();
		int[] array = new int[arrayLength];

		for (int i = 0; i < arrayLength; i++) {
			array[i] = scanner.nextInt();
		}

		int finalMinDisatnce = Integer.MAX_VALUE;
		int currentMinDistance = 0;

		for (int i = 0; i < arrayLength; i++) {
			for (int j = i + 1; j < arrayLength; j++) {
				if (array[i] == array[j]) {
					currentMinDistance = Math.abs(j - i);
					if (currentMinDistance < finalMinDisatnce) {
						finalMinDisatnce = currentMinDistance;
					}
				}

			}

		}
		System.out.println(finalMinDisatnce != Integer.MAX_VALUE ? finalMinDisatnce : -1);
	}
}
