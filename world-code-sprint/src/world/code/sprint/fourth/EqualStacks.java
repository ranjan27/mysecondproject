package world.code.sprint.fourth;

import java.util.Scanner;

public class EqualStacks {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		int totalCylinderInS1 = scanner.nextInt();
		int totalCylinderInS2 = scanner.nextInt();
		int totalCylinderInS3 = scanner.nextInt();

		int[] arrayS1 = new int[totalCylinderInS1];
		int[] arrayS2 = new int[totalCylinderInS2];
		int[] arrayS3 = new int[totalCylinderInS3];
		int sum1 = 0;
		int sum2 = 0;
		int sum3 = 0;

		for (int i = 0; i < totalCylinderInS1; i++) {
			arrayS1[i] = scanner.nextInt();
			sum1 += arrayS1[i];
		}
		for (int i = 0; i < totalCylinderInS2; i++) {
			arrayS2[i] = scanner.nextInt();
			sum2 += arrayS2[i];
		}
		for (int i = 0; i < totalCylinderInS3; i++) {
			arrayS3[i] = scanner.nextInt();
			sum3 += arrayS3[i];
		}

		int indexS1 = 0, indexS2 = 0, indexS3 = 0;
		int[] result = new int[3];

		// initialize result
		result[0] = sum1;
		result[1] = sum2;
		result[2] = sum3;

		while (indexS1 < totalCylinderInS1-1 && indexS2 < totalCylinderInS2-1
				&& indexS3 < totalCylinderInS3-1) {

			if (allEquals(result)) {
				break;
			}

			int minNumberIndex = calculateMinIndexFromResult(result);

			if (minNumberIndex == 0) {
				if (result[minNumberIndex] != result[1]) {
					result[1] = result[1] - arrayS2[indexS2];
					indexS2++;
				}
				if (result[minNumberIndex] != result[2]) {
					result[2] = result[2] - arrayS3[indexS3];
					indexS3++;
				}

			}

			if (minNumberIndex == 1) {
				if (result[minNumberIndex] != result[0]) {
					result[0] = result[0] - arrayS1[indexS1];
					indexS1++;
				}
				if (result[minNumberIndex] != result[2]) {
					result[2] = result[2] - arrayS3[indexS3];
					indexS3++;
				}

			}

			if (minNumberIndex == 2) {
				if (result[minNumberIndex] != result[0]) {
					result[0] = result[0] - arrayS1[indexS1];
					indexS1++;
				}
				if (result[minNumberIndex] != result[1]) {
					result[1] = result[1] - arrayS2[indexS2];
					indexS2++;
				}

			}
		}
		if(allEquals(result)){
			System.out.println(result[0]);
		}
		else{
			System.out.println(0);
		}
	}

	private static boolean allEquals(int[] result) {

		if (result[0] == result[1] && result[0] == result[2])
			return true;
		return false;
	}

	private static int calculateMinIndexFromResult(int[] result) {

		int minNum = result[0];
		int minIndex = 0;
		for (int i = 1; i < result.length; i++) {
			if (result[i] < minNum) {
				minNum = result[i];
				minIndex = i;
			}
		}
		return minIndex;
	}
}
