package world.code.sprint.fourth;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class HashSetApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Set<A> set = new HashSet<A>();
		
		char[] cha = "abcaa".toCharArray();
		Set<Character> set1 = new HashSet<Character>();
		
		for(char ch1:cha){
			set1.add(ch1);
		}
		
		System.out.println(set1.size());

		for (int i = 0; i < 10; i++) {
			set.add(new A());
		}

		System.out.println(set.size());
	}
}

class A {

	private int x;
	

	public int getX() {
		return x;
	}
	
	

	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		Random random = new Random();
		//return randomGenerator.nextInt(100);
		return 1;
	}

	@Override
	public boolean equals(Object obj) {
		
		return true;
	}
	
	

}
